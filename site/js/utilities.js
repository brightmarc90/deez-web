// fonction de construction d'un élement en un seul coup
function buildElmt (elmt, id, classes, href, src, dateTime, alt, width, height, children) {	
	const element = document.createElement(elmt);
	if (id) {
		element.setAttribute("id", id);
	}
	if (classes) {
		element.setAttribute("class", classes);
	}
	if (href) {
		element.setAttribute("href", href);
	}
	if (src) {
		element.setAttribute("src", src);
	}
	if (alt) {
		element.setAttribute("alt", alt);
	}
	if (width) {
		element.setAttribute("width", width);
	}
	if (height) {
		element.setAttribute("height", height);
	}
	if (dateTime) {
		element.setAttribute("dateTime", dateTime);
	}
    if(children){
        for (let i = 0;  i < array.length; i++) {
            element.appendChild(children[i]);       
        }
    }
	return element;
}

const PROXY_LINK = "https://proxy-api-dm2.herokuapp.com/";
// construction de l'url selon les options de recherche
function urlBySortOpt (inputVal, selectVal) {
	let url = "";
	switch (selectVal) {
		case "1": //album
			url = `${PROXY_LINK}https://api.deezer.com/search?q=${inputVal}&order=ALBUM_ASC&limit=80`;
			break;
		case "2": //artiste
			url = `${PROXY_LINK}https://api.deezer.com/search?q=${inputVal}&order=ARTIST_ASC&limit=80`;
			break;
		case "3": //musique
			url = `${PROXY_LINK}https://api.deezer.com/search?q=${inputVal}&order=TRACK_ASC&limit=80`;
			break;
		case "4": //populaires
			url = `${PROXY_LINK}https://api.deezer.com/search?q=${inputVal}&order=RANKING&limit=80`;
			break;
		case "5": //mieux notés
			url = `${PROXY_LINK}https://api.deezer.com/search?q=${inputVal}&order=RATING_ASC&limit=80`;
			break;
		default:
			url = `${PROXY_LINK}https://api.deezer.com/search?q=${inputVal}&limit=80`;
			break;
	}
	return url;
}

// fonction de conversion seconde en hh:mm:ss
function secondToHMS (arg) {
	let hms = ""
	let heure = Math.floor(arg / 3600) ;	
	hms = (heure == 0)? "" : ((heure < 10)? "0"+heure+":" : heure+":");
	let min = Math.floor((arg % 3600) / 60);
	hms += (min == 0)? "00:" : ((min < 10)? "0"+min+":" : min+":");
	let sec = Math.ceil((arg % 3600) % 60);
	hms += (sec < 10)? "0"+sec : sec;
	return hms;
}

const FAV_LIST = "fav-list"
// actions au clic sur un bouton fav
function favTrack (arg) {
	arg.preventDefault();
	const element = arg.currentTarget.getAttribute("id");
	let favTab;
	if(typeof localStorage !='undefined') {
		const isInLS = JSON.parse(localStorage.getItem(FAV_LIST));
		if(isInLS) {
			if(isInLS.find(e => e.id == element)){
				/* favTab = JSON.parse(localStorage.getItem(FAV_LIST));
				favTab.splice(favTab.indexOf(favTab.find(e => e.id == element)), 1);
				localStorage.setItem(FAV_LIST, JSON.stringify(favTab)); */
				alert("Cette chanson se trouve déjà dans vos favoris");
			}else{
				const requestUrl = `${PROXY_LINK}https://api.deezer.com/track/${element}`;
				// lancement de la requête et recuperation de la réponse			
				fetch(requestUrl)
					.then(response=>response.json())
					.then(result=>{
						favTab = JSON.parse(localStorage.getItem(FAV_LIST));
						favTab.push(result)
						localStorage.setItem(FAV_LIST, JSON.stringify(favTab));
						alert("Chanson ajoutée à vos vos favoris")
					})
			}
		}else{
			const requestUrl = `${PROXY_LINK}https://api.deezer.com/track/${element}`;
			// lancement de la requête et recuperation de la réponse			
			fetch(requestUrl)
				.then(response=>response.json())
	  			.then(result=>{
					favTab = [result];
					localStorage.setItem(FAV_LIST, JSON.stringify(favTab));
					alert("Chanson ajoutée à vos vos favoris")
				})
		}  		
	} else {
		alert("localStorage n'est pas supporté");
	}
}

// action du menu burger
const menuBurger = document.querySelector(".menu-burger");
menuBurger.addEventListener("click", () => {	
	var x = document.querySelector("#navigation");
	if (x.style.display === "block") {
		x.style.display = "none";
	} else {
		x.style.display = "block";
	}
})