// Construction de la section identité de l'artiste
function artistIdBuild (arg) {
	// selection du conteneur des infos sur l'artiste
	const artistIdCtnr = document.querySelector(".artist-card");
    artistIdCtnr.innerHTML = "";
        // construction de l'image
        console.log(arg)
        const image = buildElmt("img", "", "", "", arg.picture_medium, "", "photo de "+arg.name, "250", "250")
        // construction du figcaption
        const figcaption = buildElmt("figcaption");
            const h1 = buildElmt("h1", "", "mt-4");
            h1.textContent = arg.name;
            const p1 = buildElmt("p", "", "stats");
            p1.textContent = new Intl.NumberFormat('fr-FR').format(arg.nb_fan)+" fans · "+new Intl.NumberFormat('fr-FR').format(arg.nb_album)+" albums";
            const p2 = buildElmt("p");
                const a = buildElmt("a", "", "", arg.link);
                a.textContent = "Page de l'artiste";
            p2.appendChild(a);
        figcaption.appendChild(h1);
        figcaption.appendChild(p1);
        figcaption.appendChild(p2);
    artistIdCtnr.appendChild(image);
    artistIdCtnr.appendChild(figcaption);
}

// Actions au chargement de la page
window.onload = function () {
	// recuperation de l'id de l'arstist
	const idArtist = new URLSearchParams(location.search).get("id_artist");
	// construction de l'url
	if (idArtist != "") {
		const requestUrl = `${PROXY_LINK}https://api.deezer.com/artist/${idArtist}`;
		// lancement de la requête et recuperation de la réponse
		fetch(requestUrl)
			.then(response=>response.json())
  			.then(result=>{// reponse de la requête
  			artistIdBuild(result); // construction de la section 1
  		})		
	} else {
		alert("Oups! une erreur s'est produite pendant votre recherche\n Veuillez réessayer");
        location.href = "index.html";
	}
}
