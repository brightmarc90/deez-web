// construction de la section album infos
function albumInfoBuilder (arg) {
	// selection des conteneurs des infos
	const div1 = document.querySelector("#album-head .ah-img") ;
    div1.innerHTML = ""
        //image
        const img1 = buildElmt("img", "", "", "", arg.cover_big);
    div1.appendChild(img1);
    const div2 = document.querySelector("#album-head .ah-infos");
        div2.innerHTML = "";
        // etiquette album
        const p1 = buildElmt("p");
        p1.textContent = "Album"
        // tirede l'album
        const h1 = buildElmt("h1");
        h1.textContent = arg.title
        // nb titre, année de sortie, durée
        const p2 = buildElmt("p", "", "mt-2");
        p2.textContent = new Date(arg.release_date).getFullYear()+" - "+arg.nb_tracks+" titres"+" - "+secondToHMS(arg.duration);
        // artiste miniature
        const div3 = buildElmt("div", "", "mt-3");
            //figure
            const figure = buildElmt("figure", "", "d-flex align-items-center");
                const img2 = buildElmt("img", "", "", "", arg.artist.picture_medium, "", "photo de l'artiste "+arg.artist.name, "80");
                //figcation
                const figcaption = buildElmt("figcaption");
                    const p3 = buildElmt("p");
                        const a = buildElmt("a", "", "", "artist.html?id_artist="+arg.artist.id);
                        a.textContent = arg.artist.name
                    p3.appendChild(a)
                figcaption.appendChild(p3)
            figure.appendChild(img2);
            figure.appendChild(figcaption)
        div3.appendChild(figure);
    div2.appendChild(p1);
    div2.appendChild(h1);
    div2.appendChild(p2);
    div2.appendChild(div3);
}

// construction de la section liste des titres
function trackListBuilder (arg) {
    // construction du bouton ecouter sur deezer
    const btnCtnr = document.querySelector("#album-tracklist .at-link")
    btnCtnr.innerHTML = ""
        const btn = buildElmt("a", "", "", arg.link)
            const i = buildElmt("i", "", "fa-brands fa-deezer")
        btn.appendChild(i)
        btn.innerHTML += "Ecouter sur Deezer"
    btnCtnr.appendChild(btn)
	// recuperation du corps du tableau
	const tBody = document.querySelector("#album-tracklist table tbody");
    tBody.innerHTML = ""
    arg.tracks.data.forEach((element, ind) => {
        // ligne du tableau
        const tr = buildElmt("tr")
            const td1 = buildElmt("td") //col1
            td1.textContent = ind+1
            const td2 = buildElmt("td") //col2
                const i2 = buildElmt("i", "", "fa-solid fa-play d-none")
            td2.appendChild(i2)
            const td3 = buildElmt("td") //col3
                const a3 = buildElmt("a", "", "", "track.html?id_track="+element.id)
                a3.textContent = element.title
            td3.appendChild(a3)
            const td4 = buildElmt("td") //col4
                const favBtn = buildElmt("button", element.id, "btn-fav d-none");
                    const i3 = buildElmt("i", "", "fa-solid fa-heart" );
                favBtn.appendChild(i3);
            td4.appendChild(favBtn);
            const td5 = buildElmt("td")
            td5.textContent = secondToHMS(element.duration)
        tr.appendChild(td1);
        tr.appendChild(td2);
        tr.appendChild(td3);
        tr.appendChild(td4);
        tr.appendChild(td5);
        tBody.appendChild(tr);
    });
}

// Actions au chargement de la page
window.onload = function () {
	// recuperation de l'id de l'album
	const idAlbum = new URLSearchParams(location.search).get("id_album");
	// recupération de l'url
	if (idAlbum != "") {
		const requestUrl = `${PROXY_LINK}https://api.deezer.com/album/${idAlbum}`;
		// lancement de la requête et recuperation de la réponse
		fetch(requestUrl)
			.then(response=>response.json())
  			.then(result=>{// reponse de la requête  	
                console.log(result);	
  			albumInfoBuilder(result); // construction de la section 1
  			trackListBuilder(result); // construction de la section 2
  			// recupération des boutons favoris de la page
			const favBtns = document.querySelectorAll(".btn-fav");
			// écouteur d'evenement sur les boutons fav
			favBtns.forEach( function(elmt, index) {
				elmt.addEventListener("click", favTrack, false);
				// elmt.addEventListener("click", changeBtnClr, false);
			});
  		})		
	} else {
		alert("Oups! une erreur s'est produite pendant votre recherche\n Veuillez réessayer");
	}
}