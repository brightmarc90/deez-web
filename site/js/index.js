// fonction de construction des cards des tracks
function cardBuilder (arg) {
	// recuperation du conteneur principal des cards
	const resultCtnr = document.querySelector("#results-box .result-list");
    // vidage du conteneur principal
    resultCtnr.innerHTML = "";
	// boucle de construction des cards
    arg.forEach((element, index) => {  
        // console.log(element)      
        const listItem = buildElmt("li");
            // figure
            const figure = buildElmt("figure", "", "mak-card");
                // lien de la carte
                const a1 = buildElmt("a", "", "", "track.html?id_track="+element.id);
                    const image = buildElmt("img", "", "mak-card-img", "", element.album.cover_big, "", "cover de l'album "+element.album.title, "250", "280");
                a1.appendChild(image);
                // les 3 boutons
                const a2 = buildElmt("a", "", "btn-card btn-play", "track.html?id_track="+element.id);
                    const i2 = buildElmt("i", "", "fa-solid fa-play");
                a2.appendChild(i2);
                const a3 = buildElmt("a", "", "btn-card btn-disc d-none", "album.html?id_album="+element.album.id);
                    const i3 = buildElmt("i", "", "fa-solid fa-compact-disc");
                a3.appendChild(i3);
                const a4 = buildElmt("a", element.id, "btn-card btn-fav d-none", "#");
                    const i4 = buildElmt("i", "", "fa-solid fa-heart");
                a4.appendChild(i4);
                // figcaption
                const figcaption = buildElmt("figcaption", "", "mak-card-desc");
                    const div1 = buildElmt("div");
                        const img1 = buildElmt("img", "", "", "", element.artist.picture_small, "", "photo de "+element.artist.name, "60", "60");
                    div1.appendChild(img1);
                    const div2 = buildElmt("div");
                        const p1 = buildElmt("p", "", "desc-title", "", element.artist);
                        p1.textContent = element.title_short.length>15? element.title_short.substring(0, 15)+"..." : element.title_short;
                        p1.setAttribute("title", element.title);
                        const p2 = buildElmt("p", "", "desc-album");
                        const albumTitle = element.album.title.length>10? element.album.title.substring(0, 10)+"..." : element.album.title;
                        p2.textContent = secondToHMS(element.duration)+" - "+albumTitle;
                        p2.setAttribute("title", element.album.title)
                        const p3 = buildElmt("p", "", "desc-artist");
                            const a5 = buildElmt("a", "", "", "artist.html?id_artist="+element.artist.id);
                            a5.textContent = element.artist.name.length>17? element.artist.name.substring(0, 17)+"..." : element.artist.name;
                            a5.setAttribute("title", element.artist.name);
                        p3.appendChild(a5);
                    div2.appendChild(p1);
                    div2.appendChild(p2);
                    div2.appendChild(p3);
                figcaption.appendChild(div1);
                figcaption.appendChild(div2);
            figure.appendChild(a1)
            figure.appendChild(a2);
            figure.appendChild(a3);
            figure.appendChild(a4);
            figure.appendChild(figcaption)
        listItem.appendChild(figure) 
        resultCtnr.appendChild(listItem);
    });
}

// recupération du formulaire
const formEmlt = document.querySelector("#search-box form");
// récupération de titre secondaire de la page de recherche
const headText = document.querySelector("#results-box h1+p");

// Actions à la soumission du formulaire
formEmlt.addEventListener("submit", (event) => {
	// pour éviter l'envoi des paramètre via l'url
	event.preventDefault();
	// récupération des options de recherche
	const searchInput = document.querySelector("#search-input");
	//console.log(searchInput);
	const sortSelect = document.querySelector("#search-options");	
	// recupération de l'url
	if (searchInput.value != "") {
		const requestUrl = urlBySortOpt(searchInput.value, sortSelect.value);
		// lancement de la requête et recuperation de la réponse
        try {
            fetch(requestUrl)
                .then(response=>response.json())
                .then(results=>{// reponse de la requête  		
                    if(results.data.length !== 0){
                        headText.textContent = "Résultats de votre recherche";
                        cardBuilder(results.data); // construction des cards grace au tableau de resultats
                        // recupération des boutons favoris de la page
                        const favBtns = document.querySelectorAll(".btn-fav");
                        // écouter d'evenement sur les boutons fav
                        favBtns.forEach( (elmt, index) => {
                            elmt.addEventListener("click", favTrack, false);
                            // elmt.addEventListener("click", changeBtnClr, false);
                        });
                    }
                    else{
                        headText.textContent = "Aucun résultat pour votre recherche...";
                        // recuperation du conteneur principal des cards
                        const resultCtnr = document.querySelector("#results-box .result-list");
                        // vidage du conteneur principal
                        resultCtnr.innerHTML = "";
                    }              
            })
        } catch (error) {
            alert("Un prolème est survenu lors de votre recherche, veuillez réessayer")
            console.log("erreur:", error.message)
        }
	} else {
		alert("Merci de préciser l'entité à rechercher");
	}
})
