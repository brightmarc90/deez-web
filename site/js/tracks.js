// construction du bloc player
function playerBuilder (arg) {
	// recuperation du conteneur du player
	const playerCtnr = document.querySelector(".mak-player");
    playerCtnr.innerHTML = ""
	// construction du bloc de l'image et du lecteur
    const image = buildElmt("img", "", "", "", arg.album.cover_xl, "", "cover de l'album "+arg.album.title);
    const audio = buildElmt("audio", "", "", "", arg.preview);
    audio.setAttribute("controls", true);
    playerCtnr.appendChild(image);
    playerCtnr.appendChild(audio);
}

// construction du bloc track infos
function trackInfosBuilder (arg) {
	// recuperation du conteneur du player
	const trackInfoCtnr = document.querySelector(".track-infos");
    trackInfoCtnr.innerHTML = "";
	// titre de la chanson
    const h1 = buildElmt("h1")
    h1.textContent = arg.title;
	// la rangée
	const div1 = buildElmt("div", "", "row");
		const p1 = buildElmt("p", "", "timing col-12 col-md-6")
		p1.textContent = secondToHMS(arg.duration)+" - "+new Date(arg.release_date).toLocaleDateString("fr-FR", {year: 'numeric', month: 'long', day: 'numeric'});
		const div2 = buildElmt("div", "", "links col-12 col-md-6 d-flex");
			//les boutons
			const a1 = buildElmt("a", arg.id, "btn-fav me-4", "#");			
				const i1 = buildElmt("i", "", "fa-solid fa-heart");			
			a1.appendChild(i1);
			a1.innerHTML += "Ajouter au favoris"
			const a2 = buildElmt("a", "", "", arg.link);			
				const i2 = buildElmt("i", "", "fa-brands fa-deezer");
			a2.appendChild(i2);
			a2.innerHTML += "Ecouter sur Deezer"
		div2.appendChild(a1);
		div2.appendChild(a2);
	div1.appendChild(p1)
	div1.appendChild(div2)
    trackInfoCtnr.appendChild(h1);
	trackInfoCtnr.appendChild(div1);
}

// construction du bloc artist infos
function artistInfosBuilder (arg) {
	// recuperation du conteneur de l'artiste
	const artistInfoCtnr = document.querySelector(".artist-infos");
    artistInfoCtnr.innerHTML = "";
	//les 2 colonnes
	const div1 = buildElmt("div", "", "col-8");
		const figure = buildElmt("figure", "", "d-flex");
			const img = buildElmt("img", "", "", "", arg.artist.picture_medium, "", "photo de "+arg.artist.name, "60", "60");
			const figcaption = buildElmt("figcaption");
				const p1 = buildElmt("p");
					const a1 = buildElmt("a", "", "", "artist.html?id_artist="+arg.artist.id);
					a1.textContent = arg.artist.name;
				p1.appendChild(a1)
				const p2 = buildElmt("p");
				p2.textContent = "1 706 251 flws";
			figcaption.appendChild(p1);
			figcaption.appendChild(p2);
		figure.appendChild(img);
		figure.appendChild(figcaption);
	div1.appendChild(figure);
	const div2 = buildElmt("div", "", "col-4 d-flex align-items-center");
		const a2 = buildElmt("a", "", "view-pg", arg.artist.link);
		a2.textContent = "Voir la page"
	div2.appendChild(a2);
	artistInfoCtnr.appendChild(div1);
	artistInfoCtnr.appendChild(div2);
}

// construction du bloc des tracks de l'album
function albumInfosBuilder (arg) {
	// recuperation du conteneur de la liste des tracks
	const albumInfoCtnr = document.querySelector(".related-album");
    albumInfoCtnr.innerHTML = "";
	// related album head
	const div1 = buildElmt("div", "", "mak-head px-3 py-2");
		const h2 = buildElmt("h2");
		h2.textContent = arg.album.title;
		const p = buildElmt("p");
		p.textContent = "Album";
	div1.appendChild(h2);
	div1.appendChild(p);
	const div2 = buildElmt("div", "", "mak-img");
		const img = buildElmt("img", "", "", "", arg.album.cover_big, "", "photo de l'album "+arg.album.name);
	div2.appendChild(img);
	const ul = buildElmt("ul", "", "track-list list-unstyled py-3");
	fetch(PROXY_LINK+arg.album.tracklist)
		.then(response => response.json())
		.then(result => {
			result.data.forEach(element => {
				const li = buildElmt("li");
					const a = buildElmt("a", "", "", "track.html?id_track="+element.id);
						const span1 = buildElmt("span")
						span1.textContent = element.title_short;
						const span2 = buildElmt("span")
						span2.textContent = secondToHMS(element.duration)
					a.appendChild(span1);
					a.appendChild(span2);
				li.appendChild(a);
				ul.appendChild(li);
			});
		})
	
	albumInfoCtnr.appendChild(div1);
	albumInfoCtnr.appendChild(div2);
	albumInfoCtnr.appendChild(ul)
}

// Actions au chargement de la page
window.onload = function () {
	// recuperation de l'id du track
	const idTrack = new URLSearchParams(location.search).get("id_track");
	// construction de l'url
	if (idTrack != "") {
		const requestUrl = `${PROXY_LINK}https://api.deezer.com/track/${idTrack}`;
		// lancement de la requête et recuperation de la réponse
		fetch(requestUrl)
			.then(response=>response.json())
  			.then(result=>{// reponse de la requête  			  				
                // console.log(result);
  			    playerBuilder(result); // construction du player
  			    trackInfosBuilder(result); // construction de la section trackinfos
				artistInfosBuilder(result) // construction du bloc artist infos
				albumInfosBuilder(result) // construction du bloc album infos
  			// recupération des boutons favoris de la page
			const favBtn = document.querySelector(".btn-fav");
			// écouter d'evenement sur les boutons fav
			favBtn.addEventListener("click", favTrack, false);
			// favBtn.addEventListener("click", changeBtnClr, false);
  		})		
	} else {
		alert("Oups! une erreur s'est produite pendant votre recherche\n Veuillez réessayer");
	}
}