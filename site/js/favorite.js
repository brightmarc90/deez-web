// construction du corps du tableau
function favListBuild (arg, ind) {
	// selection du corps du tableau;
	const tBody = document.querySelector("#fav-list table tbody");
        // ligne du tableau
        const tr = buildElmt("tr")
            const td1 = buildElmt("td") // col1
            td1.textContent = ind;
            const td2 = buildElmt("td") // col2
                const img = buildElmt("img", "", "", "", arg.album.cover_medium, "", "", "40")
            td2.appendChild(img)
            const td3 = buildElmt("td") // col3
                const a1 = buildElmt("a", "", "", "track.html?id_track="+arg.id);
                a1.textContent = arg.title
                const small = buildElmt("small")
                small.textContent = arg.artist.name;
            td3.appendChild(a1);
            td3.appendChild(small);
            const td4 = buildElmt("td") // col4
            td4.textContent = arg.album.title
            const td5 = buildElmt("td") // col5
                const btn = buildElmt("button", arg.id, "btn-del d-none");
                    const icon = buildElmt("i", "", "fa-solid fa-trash");
                btn.appendChild(icon);
            td5.appendChild(btn);
            const td6 = buildElmt("td") // col6
            td6.textContent = secondToHMS(arg.duration);
        tr.appendChild(td1);
        tr.appendChild(td2)
        tr.appendChild(td3)
        tr.appendChild(td4)
        tr.appendChild(td5)
        tr.appendChild(td6)
    tBody.appendChild(tr);
}
// Actions au chargement de la page
window.onload = function () {
	// parcours du localStorage et affichage des tracks
	if(typeof localStorage !='undefined') {
        const favTab = JSON.parse(localStorage.getItem(FAV_LIST));
        if(favTab){
            favTab.forEach((element, ind) => {
                favListBuild(element, ind+1);
            });
        }
		// recupération des boutons favoris de la page
		const delBtns = document.querySelectorAll(".btn-del");
		// écouteur d'evenement sur les boutons fav
		delBtns.forEach( (elmt, index) => {
			// elmt.addEventListener("click", favoriteTrack, false);
			elmt.addEventListener("click", delTrack, false);
		});
	} else {
		alert("localStorage n'est pas supporté");
	}	
}

// suppression du track de la liste
function delTrack (arg) {
    arg.preventDefault();
	const element = arg.currentTarget.getAttribute("id");
	let favTab = JSON.parse(localStorage.getItem(FAV_LIST));
    favTab.splice(favTab.indexOf(favTab.find(e => e.id == element)), 1);
    localStorage.setItem(FAV_LIST, JSON.stringify(favTab));

	const btnParent = arg.currentTarget.parentNode.parentNode;
	// selection du corps du tableau;
	const tBody = document.querySelector("#fav-list table tbody");
	// suppression de la ligne
	tBody.removeChild(btnParent);
    alert("Cette chanson a été retirée de vos favoris")
    location.href = "favorite.html";
}